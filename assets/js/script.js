// Author: Jacques Dhuesme
// Date: 2023-02-14

// Global variables
const newgamebutton = document.getElementById('newGame');
const holdbutton = document.getElementById('holdbutton');
const rollbutton = document.getElementById('rollbutton');

const pinplayer1 = document.getElementById('player1');
const pinplayer2 = document.getElementById('player2');

const gameValue = {
    player1: {
        score: 0,
        currentScore: 0,
    },
    player2: {
        score: 0,
        currentScore: 0,
    },
    currentPlayer: 'player1',
};

// Functions
// function newGame - reset all values and start a new game
function newGame() {
    gameValue.player1.score = 0;
    gameValue.player2.score = 0;
    gameValue.player1.currentScore = 0;
    gameValue.player2.currentScore = 0;
    gameValue.currentPlayer = 'player1';

    document.getElementById("player1Score").innerHTML = gameValue.player1.score;
    document.getElementById("player2Score").innerHTML = gameValue.player2.score;
    document.getElementById("player1CurrentScore").innerHTML = gameValue.player1.currentScore;
    document.getElementById("player2CurrentScore").innerHTML = gameValue.player2.currentScore;

    pinplayer1.style.visibility = "visible";
    pinplayer2.style.visibility = "hidden";

    rollbutton.disabled = false;
    holdbutton.disabled = false;
}

// function changePlayer - change the current player
function changePlayer() {
    if( checkWinner() ) return;
    if (gameValue.currentPlayer === 'player1') {
        gameValue.currentPlayer = 'player2';
        pinplayer1.style.visibility = "hidden";
        pinplayer2.style.visibility = "visible";
    } else {
        gameValue.currentPlayer = 'player1';
        pinplayer1.style.visibility = "visible";
        pinplayer2.style.visibility = "hidden";
    }
}

// function rollDice - roll the dice and add the value to the current score
function rollDice() {
    let dice = Math.floor(Math.random() * 6) + 1;
    switch (dice) {
        case 1:
            document.getElementById("dot1").style.visibility = "hidden";
            document.getElementById("dot2").style.visibility = "hidden";
            document.getElementById("dot3").style.visibility = "hidden";
            document.getElementById("dot4").style.visibility = "hidden";
            document.getElementById("dot5").style.visibility = "visible";
            document.getElementById("dot6").style.visibility = "hidden";
            document.getElementById("dot7").style.visibility = "hidden";
            document.getElementById("dot8").style.visibility = "hidden";
            document.getElementById("dot9").style.visibility = "hidden";
            break;
        case 2:
            document.getElementById("dot1").style.visibility = "hidden";
            document.getElementById("dot2").style.visibility = "hidden";
            document.getElementById("dot3").style.visibility = "visible";
            document.getElementById("dot4").style.visibility = "hidden";
            document.getElementById("dot5").style.visibility = "hidden";
            document.getElementById("dot6").style.visibility = "hidden";
            document.getElementById("dot7").style.visibility = "visible";
            document.getElementById("dot8").style.visibility = "hidden";
            document.getElementById("dot9").style.visibility = "hidden";
            break;
        case 3:
            document.getElementById("dot1").style.visibility = "hidden";
            document.getElementById("dot2").style.visibility = "hidden";
            document.getElementById("dot3").style.visibility = "visible";
            document.getElementById("dot4").style.visibility = "hidden";
            document.getElementById("dot5").style.visibility = "visible";
            document.getElementById("dot6").style.visibility = "hidden";
            document.getElementById("dot7").style.visibility = "visible";
            document.getElementById("dot8").style.visibility = "hidden";
            document.getElementById("dot9").style.visibility = "hidden";
            break;
        case 4:
            document.getElementById("dot1").style.visibility = "visible";
            document.getElementById("dot2").style.visibility = "hidden";
            document.getElementById("dot3").style.visibility = "visible";
            document.getElementById("dot4").style.visibility = "hidden";
            document.getElementById("dot5").style.visibility = "hidden";
            document.getElementById("dot6").style.visibility = "hidden";
            document.getElementById("dot7").style.visibility = "visible";
            document.getElementById("dot8").style.visibility = "hidden";
            document.getElementById("dot9").style.visibility = "visible";
            break;
        case 5:
            document.getElementById("dot1").style.visibility = "visible";
            document.getElementById("dot2").style.visibility = "hidden";
            document.getElementById("dot3").style.visibility = "visible";
            document.getElementById("dot4").style.visibility = "hidden";
            document.getElementById("dot5").style.visibility = "visible";
            document.getElementById("dot6").style.visibility = "hidden";
            document.getElementById("dot7").style.visibility = "visible";
            document.getElementById("dot8").style.visibility = "hidden";
            document.getElementById("dot9").style.visibility = "visible";
            break;
        case 6:
            document.getElementById("dot1").style.visibility = "visible";
            document.getElementById("dot2").style.visibility = "hidden";
            document.getElementById("dot3").style.visibility = "visible";
            document.getElementById("dot4").style.visibility = "visible";
            document.getElementById("dot5").style.visibility = "hidden";
            document.getElementById("dot6").style.visibility = "visible";
            document.getElementById("dot7").style.visibility = "visible";
            document.getElementById("dot8").style.visibility = "hidden";
            document.getElementById("dot9").style.visibility = "visible";
            break;
    }
    if (dice !== 1) {
        gameValue[gameValue.currentPlayer].currentScore += dice;
        document.getElementById(`${gameValue.currentPlayer}CurrentScore`).innerHTML = gameValue[gameValue.currentPlayer].currentScore;
    } else {
        gameValue[gameValue.currentPlayer].currentScore = 0;
        document.getElementById(`${gameValue.currentPlayer}CurrentScore`).innerHTML = gameValue[gameValue.currentPlayer].currentScore;
        changePlayer();
    }
}

// check if the player has won
function checkWinner() {
    if (gameValue[gameValue.currentPlayer].score >= 100) {
        document.getElementById(`${gameValue.currentPlayer}Score`).innerHTML = 'WINNER!';
        document.getElementById(`${gameValue.currentPlayer}CurrentScore`).innerHTML = 'WINNER!';
        rollbutton.disabled = true;
        holdbutton.disabled = true;
        return true;
    }
    return false;
}
//Event listeners
// new game button
newgamebutton.addEventListener('click', function(){
    newGame();
});

// roll dice button
rollbutton.addEventListener('click', function(){
    rollDice();
});

// hold button
holdbutton.addEventListener('click', function(){
    gameValue[gameValue.currentPlayer].score += gameValue[gameValue.currentPlayer].currentScore;
    document.getElementById(`${gameValue.currentPlayer}Score`).innerHTML = gameValue[gameValue.currentPlayer].score;
    gameValue[gameValue.currentPlayer].currentScore = 0;
    document.getElementById(`${gameValue.currentPlayer}CurrentScore`).innerHTML = gameValue[gameValue.currentPlayer].currentScore;
    changePlayer();
});

// check if the DOM is loaded and then start the game
if (document.readyState === 'complete') {
  newGame();
} else {
  document.addEventListener('DOMContentLoaded', function() {
    newGame();
  });
}